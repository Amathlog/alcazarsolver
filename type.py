""" Static class
Store the values of the nodes, determining their type
"""
class Type(object):

    empty = 0b00000
    leftWall = 0b00001
    topWall = 0b00010
    rightWall = 0b00100
    bottomWall = 0b01000

    horizontalLine = 1  # ─    
    verticalLine = 2    #  |
    leftTop = 3         # ┘
    rightTop = 4        # └
    rightBottom = 5     # ┌
    leftBottom = 6      # ┐

    @staticmethod
    def hasLeftWall(value):
        return value % 2

    @staticmethod
    def hasTopWall(value):
        return (value>>1) % 2

    @staticmethod
    def hasRightWall(value):
        return (value>>2) % 2

    @staticmethod
    def hasBottomWall(value):
        return (value>>3) % 2

    @staticmethod
    def isBlock(value):
        return  Type.hasBottomWall(value) and \
                Type.hasRightWall(value) and \
                Type.hasTopWall(value) and \
                Type.hasLeftWall(value)

    @staticmethod
    def isExit(value):
        return Type.hasBottomWall(value) and \
                Type.hasRightWall(value) and \
                Type.hasTopWall(value) and \
                not Type.hasLeftWall(value) or \
                \
                Type.hasBottomWall(value) and \
                Type.hasRightWall(value) and \
                not Type.hasTopWall(value) and \
                Type.hasLeftWall(value) or \
                \
                Type.hasBottomWall(value) and \
                not Type.hasRightWall(value) and \
                Type.hasTopWall(value) and \
                Type.hasLeftWall(value) or \
                \
                not Type.hasBottomWall(value) and \
                Type.hasRightWall(value) and \
                Type.hasTopWall(value) and \
                Type.hasLeftWall(value)
