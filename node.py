"""
Class Node

The Node class store the information about 1 node : value, has been visited, 
the path (node in and node out), its id and its postion in the grid
"""
class Node(object):

    __nodeCounter = 0
    
    def __init__(self, value, x, y):
        self.__value = value
        self.__visited = False
        self.__in = None
        self.__out = None
        self.__id = Node.__nodeCounter
        self.__x = x
        self.__y = y
        Node.__nodeCounter += 1

    # Getters and setters
    def getValue(self):
        return self.__value

    def setValue(self, value):
        self.__value = value

    def isVisited(self):
        return self.__visited

    def setVisited(self, value):
        self.__visited = value

    def getIn(self):
        return self.__in

    def setIn(self, node):
        self.__in = node

    def getOut(self):
        return self.__out

    def setOut(self, node):
        self.__out = node

    def getId(self):
        return self.__id

    def getX(self):
        return self.__x

    def getY(self):
        return self.__y

    """
    If it's determined, return the type of the path (─ , | , ┘, └, ┐ or ┌)
    If not, return 0
    """
    def isDetermined(self):
        from type import Type
        
        if self.__in is None or self.__out is None:
            return 0

        if self.__in.x == self.__out.x:
            return Type.horizontalLine

        if self.__in.y == self.__out.y:
            return Type.verticalLine

        if (self.__in.x == self.__x and self.__out.x == self.__x - 1) or (self.__out.x == self.__x and self.__in.x == self.__x - 1):
            if(self.__in.y == self.__y - 1 and self.__out.y == self.__y) or (self.__out.y == self.__y - 1 and self.__in.y == self.__y):
                return Type.leftTop
            else:
                return Type.rightTop

        if (self.__in.x == self.__x and self.__out.x == self.__x + 1) or (self.__out.x == self.__x and self.__in.x == self.__x + 1):
            if(self.__in.y == self.__y - 1 and self.__out.y == self.__y) or (self.__out.y == self.__y - 1 and self.__in.y == self.__y):
                return Type.leftBottom
            else:
                return Type.rightBottom

    """
    Check if one of the extremities has been determined
    Return the linked node
    """
    def isPartiallyDetermined(self):
        if self.__in is not None:
            return self.__in
        elif self.__out is not None:
            return self.__out
        else:
            return None

    """
    Overloading == operator, comparing ids (which are unique)
    """
    def __eq__(self, other):
        return self.__id == other.id

    # Properties for easier access
    value = property(getValue, setValue)
    visited = property(isVisited, setVisited)
    nodeIn = property(getIn, setIn)
    nodeOut = property(getOut, setOut)
    id = property(getId)
    x = property(getX)
    y = property(getY)

    # Overloading repr
    def __repr__(self):
        return "(Id = " + str(self.__id) + \
        ", Coordinates = (" + str(self.__x) + ", " + str(self.__y) + ")" + \
        ", Value = " + str(self.__value) + \
        ", Visited = " + ("True" if self.__visited else "False") + \
        ", Node In = " + (str(self.__in.id) if self.__in else "None") + \
        ", Node Out = " + (str(self.__out.id) if self.__out else "None") + ")"


# Test code
if __name__ == '__main__':

    from type import Type

    node1 = Node(Type.leftWall, 0, 0)
    node2 = Node(Type.rightWall + Type.topWall, 0, 1)
    node3 = Node(Type.bottomWall + Type.exit, 1, 0)
    node4 = Node(Type.bottomWall + Type.rightWall, 1, 1)

    node1.nodeOut = node2
    node2.nodeIn = node1
    node2.nodeOut = node4
    node3.nodeIn = node4
    node4.nodeIn = node2
    node4.nodeOut = node3


    print(node1)
    print(node2)
    print(node3)
    print(node4)