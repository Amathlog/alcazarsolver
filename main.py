import sys
from PyQt5.QtWidgets import QApplication
from grid import Grid

from gridView import GridView


if __name__ == '__main__':
    
    app = QApplication(sys.argv)

    levelName = "hard"

    grid = Grid(levelName + ".txt")
    
    grid.preprocessing(15)
    #gridView = GridView(grid, levelName)
    #print(grid)

    #grid.backtracking()

    gridView = GridView(grid, levelName)
    
    sys.exit(app.exec_())