import sys
from node import Node
from type import Type

"""
Class Grid

The Grid class is the core of the project
It stores the information about the level (size, data, exits etc...)
It also has the backtracking algorithm and a preprocessing method
to add constraint in order to speed up the backtracking algorithm.
"""

class Grid(object):

    """ 
    Initialize the grid object
    empty or can load a formatted file
    """
    def __init__(self, filename = None):
        self.__sizeX = 0
        self.__sizeY = 0
        self.__data = []
        self.__exits = []
        self.__accessibleBlocks = 0
        self.__currentVisitedBlocks = 0
        self.__currentVisitedExits = []
        self.__numberOfBacktrack = 0
        self.__savedExits = []
        self.__exitsDetermined = []
        if filename:
            self.readFromFile(filename)

    """
    Check if the x,y coordinates are outside the grid
    """
    def __isOutBoundries(self, x, y):
        return x < 0 or x >= self.__sizeX or y < 0 or y >= self.__sizeY

    """
    Method used for debbuging at the beggining, should not
    be used outside.
    """
    def __hardInit(self, x, y):
        self.__sizeX = x
        self.__sizeY = y
        self.__data = x*y*[None]

    # All the getters/setters
    def getSizeX(self):
        return self.__sizeX

    def getSizeY(self):
        return self.__sizeY

    def getNodes(self):
        return self.__data

    def getExits(self):
        return self.__exits

    def getItem(self, x, y):
        if self.__isOutBoundries(x,y):
            print("X and/or Y are out boundries")
            raise AttributeError
        return self.__data[x * self.__sizeY + y]

    def setItem(self, x, y, node):
        if self.__isOutBoundries(x,y):
            print("X and/or Y are out boundries")
            raise AttributeError
        if not self.__data:
            raise "The grid was not initialized"
            raise AttributeError

        self.__data[x * self.__sizeY + y] = node


    """
    Method for backtracking algorithm
    Set the node to visited and update the number
    of visited block.
    It returns also if the algorithm is finished (aka. all
    the nodes has been visited, we found a path which go
    through every node)
    """
    def __visiting(self, node):
        node.visited = True
        if(not Type.isExit(node.value)):
            self.__currentVisitedBlocks += 1
            return False
        return self.__currentVisitedBlocks == self.__accessibleBlocks

    """
    Method for backtracking algorithm
    Set the node to unvisited and update the number
    of visited block.
    It's called when we backtrack
    """
    def __unvisiting(self, node):
        node.visited = False
        if(not Type.isExit(node.value)):
            self.__currentVisitedBlocks -= 1

    """
    Method to read from a file.
    The file must be formatted under those constraints :
    First line of the file : 2 numbers sizeX and sizeY, with one space between them
    Next sizeX line : sizeY numbers, with one space between all of them

    Raise an IOError if the file do not exist and undetermined behaviour if the
    format is not respected
    """
    def readFromFile(self, filename):
        with open(filename, 'r') as f:
            if not f:
                print("The file " + filename + " is not found")
                raise IOError

            size = f.readline().split()
            self.__sizeX = int(size[0])
            self.__sizeY = int(size[1])

            for x in range(self.__sizeX):
                aux = f.readline().split()                    
                for y in range(self.__sizeY):
                    node = Node(int(aux[y]), x, y)
                    self.__data.append(node)
                    if(Type.isExit(node.value)):
                        self.__exits.append(node)
                    elif(not Type.isBlock(node.value)):
                        self.__accessibleBlocks += 1

    """
    Method to write into a file.
    Will be useful if we want to generate grids, but for now
    it is not used
    """
    def writeToFile(self, filename):
        with open(filename, 'w') as f:
            # Write the size X and Y at the beginning
            f.write(str(self.__sizeX) + ' ' + str(self.__sizeY) + '\n')

            # Then X lines of Y numbers
            aux = ""
            for x in range(self.__sizeX):
                aux += str(self.__data[x * self.__sizeY].value)
                for y in range(1, self.__sizeY):
                    aux += ' ' + str(self.__data[x * self.__sizeY + y].value)
                aux += '\n'

            f.write(aux)

    """
    Overloading the str() method. Allow easier debugging by writing
    all the information of the class grid.
    """
    def __repr__(self):
        res = "Size : (" + str(self.__sizeX) + "," + str(self.__sizeY) + ')\n'
        for item in self.__data:
            res += str(item) + '\n'
        return res


    """
    Access to neighboors. Return None if the neighboor will be outside the grid
    Can access with coordinates (x,y) and from node.
    Same thing for Right, Bottom, Left and Top
    """
    def __getRightItemXY(self, x, y):
        if y == self.__sizeY - 1:
            return None
        return self.getItem(x, y+1)

    def __getRightItem(self, node):
        if node is None:
            return None
        return self.__getRightItemXY(node.x, node.y)

    def __getBottomItemXY(self, x, y):
        if x == self.__sizeX - 1:
            return None
        return self.getItem(x+1, y)

    def __getBottomItem(self, node):
        if node is None:
            return None
        return self.__getBottomItemXY(node.x, node.y)

    def __getLeftItemXY(self, x, y):
        if y == 0:
            return None
        return self.getItem(x, y-1)

    def __getLeftItem(self, node):
        if node is None:
            return None
        return self.__getLeftItemXY(node.x, node.y)

    def __getTopItemXY(self, x, y):
        if x == 0:
            return None
        return self.getItem(x-1, y)

    def __getTopItem(self, node):
        if node is None:
            return None
        return self.__getTopItemXY(node.x, node.y)

    """
    Useful for bouncing algorithm, get the item in
    diagonal.
    Same thing for TopRight, TopLeft, BottomRight and BottomLeft 
    """
    def __getTopRightItem(self, node):
        aux = self.__getRightItem(node)
        if aux is not None:
            return self.__getTopItem(aux)

    def __getTopLeftItem(self, node):
        aux = self.__getLeftItem(node)
        if aux is not None:
            return self.__getTopItem(aux)

    def __getBottomRightItem(self, node):
        aux = self.__getRightItem(node)
        if aux is not None:
            return self.__getBottomItem(aux)

    def __getBottomLeftItem(self, node):
        aux = self.__getLeftItem(node)
        if aux is not None:
            return self.__getBottomItem(aux)

    
    """
    With new constraint, the fact that a node has a wall is not limited to having a wall itself
    but also if the neighboor has become a block (an exit is no longer accessible) or it has been
    fully determined and can be no longer reached from this node (for example if the right neighboor
    is fully determined by a vertical line (enter on bottom, exit on top or vice-versa), it cannot be reached from the left
    anymore. Therefore the right neighboor has become a wall for the current node)

    It's the same for corners and corridors, there are 2 conditions to verify for each
    """
    def __hasBottomWall(self, node):
        return Type.hasBottomWall(node.value) or \
        (self.__getBottomItem(node).isDetermined() in [Type.horizontalLine, Type.leftBottom, Type.rightBottom] )or \
        Type.isBlock(self.__getBottomItem(node).value)

    def __hasTopWall(self, node):
        return Type.hasTopWall(node.value) or \
        (self.__getTopItem(node).isDetermined() in [Type.horizontalLine, Type.leftTop, Type.rightTop] ) or \
        Type.isBlock(self.__getTopItem(node).value)

    def __hasRightWall(self, node):
        return Type.hasRightWall(node.value) or \
        (self.__getRightItem(node).isDetermined() in [Type.verticalLine, Type.rightTop, Type.rightBottom] ) or \
        Type.isBlock(self.__getRightItem(node).value)

    def __hasLeftWall(self, node):
        return Type.hasLeftWall(node.value) or \
        (self.__getLeftItem(node).isDetermined() in [Type.verticalLine, Type.leftBottom, Type.leftTop] ) or \
        Type.isBlock(self.__getLeftItem(node).value)

    def __isTopLeftCorner(self, node):
        return self.__hasLeftWall(node) and self.__hasTopWall(node)

    def __isTopRightCorner(self, node):
        return self.__hasRightWall(node) and self.__hasTopWall(node)

    def __isBottomLeftCorner(self, node):
        return self.__hasLeftWall(node) and self.__hasBottomWall(node)

    def __isBottomRightCorner(self, node):
        return self.__hasRightWall(node) and self.__hasBottomWall(node)

    def __isHorizontalCorridor(self, node):
        return self.__hasBottomWall(node) and self.__hasTopWall(node)

    def __isVerticalCorridor(self, node):
        return self.__hasLeftWall(node) and self.__hasRightWall(node)



    """
    Print the grid in ascii. Cannot show path though, therefore a GUI 
    application was made.
    """
    def printASCII(self):
        for x in range(self.__sizeX):
            res = ""
            for y in range(self.__sizeY):

                value = self.getItem(x,y).value

                if(Type.isBlock(value) and self.__getBottomItemXY(x,y) is not None and not Type.isBlock(self.__getBottomItemXY(x,y).value)) or (not Type.isBlock(value) and Type.hasBottomWall(value)):
                    res += "_"
                else:
                    res += ' '
                if(Type.isBlock(value) and self.__getRightItemXY(x,y) is not None and not Type.isBlock(self.__getRightItemXY(x,y).value)) or (not Type.isBlock(value) and Type.hasRightWall(value)):
                    res += "|"
                else:
                    res += ' '
            print(res)

    """
    Add a contraint in the preprocessing.
    This method is used when a node is now fully
    determined.
    node1 : The node in
    node2 : The new fully determined node
    node3 : The node out
    """
    def __addConstraint(self, node1, node2, node3):
        # Add the full constraint to this node
        node2.nodeIn = node1
        node2.nodeOut = node3

        # If the node out is already fully determined
        # do not change anything
        if(node3.isDetermined() == 0):
            # Add the constraint to one of the empty node in or out
            if node3.nodeIn is None and (node3.nodeOut is not None and node3.nodeOut != node2):
                node3.nodeIn = node2
            elif node3.nodeOut is None:
                node3.nodeOut = node2
        
        # If the node in is already fully determined
        # do not change anything
        if(node1.isDetermined() == 0):
            # Add the constraint to one of the empty node in or out
            if node1.nodeIn is None and (node1.nodeOut is not None and node1.nodeOut != node2):
                node1.nodeIn = node2
            elif node1.nodeOut is None:
                node1.nodeOut = node2

    """
    Add a soft contraint in the preprocessing.
    This method is used when 2 node are now
    partially determined
    node1 : The node out
    node2 : The node in
    """
    def __addSoftConstraint(self, node1, node2):
        # If the node out is already fully determined
        # do not change anything
        if (node1.isDetermined() == 0):
            # Add the constraint to one of the empty node in or out
            if node1.nodeIn is None and (node1.nodeOut is not None and node1.nodeOut != node2):
                node1.nodeIn = node2
            elif node1.nodeOut is None:
                node1.nodeOut = node2

        # If the node in is already fully determined
        # do not change anything
        if (node2.isDetermined() == 0):
            # Add the constraint to one of the empty node in or out
            if node2.nodeIn is None and (node2.nodeOut is not None and node2.nodeOut != node1):
                node2.nodeIn = node1
            elif node2.nodeOut is None :
                node2.nodeOut = node1


    """
    Preprocessing the grid.
    Used multiple times on the grid, it can add constraints in order to 
    speed up the algorithm.
    Those constraints are :
    -> Corners and corridors : If a cell is in a corner or a corridor, it is a fully
       determined behaviour
    -> Bouncing : When you have a determined corner, you can "bounce" on other cells to
       add soft constraints
       For example :

         _______
        |  __
        | |
        | |  
        |   ___

        In this configuration we have a corner on the top left, therefore the path is clear for the cell
        at the top left. But we also have a bottom wall in the bottom right diagonal of the top left cell.
        It cannot be a leftTop path, because it will create a loop in the already determined path. Therefore
        in this cell, the only path possible is to go out to(or in from) the left. We have now a soft constraint
        where 1 way (in or out) is already determined for 2 cells.

    -> Exits and grid size : There is a interesting property in this kind of problem. If you enter the grid from a
       white square, if the number of cells in the grid is even, you need to exit to a grey cell. And is the number
       is odd, you need to exit to a white cell.
       Therefore, if an exit is already determined, we can close all the exits that does not respect this property. 
       Also if 2 exits has already been detected, we can close all the other exits.

    -> Loops: One other property (which is identical to bounce) is that you can't make loops. Therefore, if there are 2
       possibilities for one node and one of them is making a loop, the only solution is the other one.
    """
    def preprocessing(self, passes = 10):
        # Check the number of passes.
        # If it's 0 we go out
        if passes <= 0:
            return

        # Save the fact that one constraint has been added
        newConstraint = False

        # Save all new corners determined in this list
        bouncingList = []


        # Corners and corridors constraints + loop
        for node in self.__data:
            # Do not preprocess already determined nodes or blocks or exits
            if(Type.isBlock(node.value) or Type.isExit(node.value) or node.isDetermined() >= 1):
                continue
            if(self.__isTopLeftCorner(node)):
                self.__addConstraint(self.__getRightItem(node), node, self.__getBottomItem(node))
                bouncingList.append((node, Type.rightBottom))
                newConstraint = True
            elif(self.__isTopRightCorner(node)):
                self.__addConstraint(self.__getBottomItem(node), node, self.__getLeftItem(node))
                bouncingList.append((node, Type.leftBottom))
                newConstraint = True
            elif(self.__isBottomRightCorner(node)):
                self.__addConstraint(self.__getLeftItem(node), node, self.__getTopItem(node))
                bouncingList.append((node, Type.leftTop))
                newConstraint = True
            elif(self.__isBottomLeftCorner(node)):
                self.__addConstraint(self.__getTopItem(node), node, self.__getRightItem(node))
                bouncingList.append((node, Type.rightTop))
                newConstraint = True
            elif(self.__isHorizontalCorridor(node)):
                self.__addConstraint(self.__getRightItem(node), node, self.__getLeftItem(node))
                newConstraint = True
            elif(self.__isVerticalCorridor(node)):
                self.__addConstraint(self.__getBottomItem(node), node, self.__getTopItem(node))
                newConstraint = True
            # Loop constraints
            # The node needs first to be partially determined
            elif(node.isPartiallyDetermined()):
                fourCoordinates = [self.__getRightItem(node), self.__getLeftItem(node),\
                                    self.__getTopItem(node), self.__getBottomItem(node)]
                # Direct Loop
                possibilities = [poss for poss in fourCoordinates if poss is not None and not poss.isDetermined() \
                                                                            and not Type.isBlock(poss.value)]
                if len(possibilities) == 2:
                    other = self.__otherExtremity(node)
                    if other in possibilities:
                        possibilities.remove(other)
                        self.__addSoftConstraint(node, possibilities[0])

                # Indirect loop
                if fourCoordinates[0] is not None and not fourCoordinates[0].isPartiallyDetermined():
                    nextStep = self.__getRightItem(fourCoordinates[0])
                    if nextStep is not None:
                        other = self.__otherExtremity(nextStep)
                        if other is not None and other == node:
                            aux = [self.__getTopItem(fourCoordinates[0]), self.__getBottomItem(fourCoordinates[0])]
                            if not aux[0].isPartiallyDetermined() and not aux[1].isPartiallyDetermined():
                                pass
                            elif aux[0].isDetermined() and not aux[1].isDetermined():
                                self.__addSoftConstraint(aux[1], fourCoordinates[0])
                                if self.__getBottomLeftItem(fourCoordinates[0]) is not None:
                                    bouncingList.append((self.__getBottomLeftItem(fourCoordinates[0]), Type.rightBottom))
                                if self.__getBottomRightItem(fourCoordinates[0]) is not None:
                                    bouncingList.append((self.__getBottomRightItem(fourCoordinates[0]), Type.leftBottom))
                            elif aux[1].isDetermined() and not aux[0].isDetermined():
                                self.__addSoftConstraint(aux[0], fourCoordinates[0])
                                if self.__getTopLeftItem(fourCoordinates[0]) is not None:
                                    bouncingList.append((self.__getTopLeftItem(fourCoordinates[0]), Type.rightTop))
                                if self.__getTopRightItem(fourCoordinates[0]) is not None:
                                    bouncingList.append((self.__getTopRightItem(fourCoordinates[0]), Type.leftTop))

        # Bouncing constraints
        while bouncingList:
            node, corner = bouncingList.pop()
            if corner == Type.rightBottom:
                aux = self.__getBottomRightItem(node)
                if aux is not None and not self.__hasLeftWall(aux) and not self.__hasTopWall(aux):
                    if self.__hasRightWall(aux):
                        self.__addSoftConstraint(aux, self.__getBottomItem(aux))
                        bouncingList.append((aux, Type.leftBottom))
                    elif self.__hasBottomWall(aux):
                        self.__addSoftConstraint(aux, self.__getRightItem(aux))
                        bouncingList.append((aux, Type.rightTop))
            if corner == Type.leftBottom:
                aux = self.__getBottomLeftItem(node)
                if aux is not None and not self.__hasTopWall(aux) and not self.__hasRightWall(aux):
                    if self.__hasLeftWall(aux):
                        self.__addSoftConstraint(aux, self.__getBottomItem(aux))
                        bouncingList.append((aux, Type.rightBottom))
                    elif self.__hasBottomWall(aux):
                        self.__addSoftConstraint(aux, self.__getLeftItem(aux))
                        bouncingList.append((aux, Type.leftTop))
            if corner == Type.leftTop:
                aux = self.__getTopLeftItem(node)
                if aux is not None and not self.__hasRightWall(aux) and not self.__hasBottomWall(aux):
                    if self.__hasLeftWall(aux):
                        self.__addSoftConstraint(aux, self.__getTopItem(aux))
                        bouncingList.append((aux, Type.rightTop))
                    elif self.__hasTopWall(aux):
                        self.__addSoftConstraint(aux, self.__getLeftItem(aux))
                        bouncingList.append((aux, Type.leftBottom))
            if corner == Type.rightTop:
                aux = self.__getTopRightItem(node)
                if aux is not None and not self.__hasLeftWall(aux) and not self.__hasBottomWall(aux):
                    if self.__hasRightWall(aux):
                        self.__addSoftConstraint(aux, self.__getTopItem(aux))
                        bouncingList.append((aux, Type.leftTop))
                    elif self.__hasTopWall(aux):
                        self.__addSoftConstraint(aux, self.__getRightItem(aux))
                        bouncingList.append((aux, Type.rightBottom))

        # Exits constraints
        # Check the number of detected exits and save them
        newConstraint = self.__exitsPreprocessing() or newConstraint

        # If a new constraint has been detected, relaunch the algorithm
        if newConstraint:
            self.preprocessing(passes-1)

    # Find the other extremity of an already determined path
    def __otherExtremity(self, start):
        if start.isDetermined() or not start.isPartiallyDetermined():
            return None

        curr = start.isPartiallyDetermined()
        previous = start
        while curr.isDetermined():
            if curr.nodeIn == previous:
                previous = curr
                curr = curr.nodeOut
            else:
                previous = curr
                curr = curr.nodeIn
        return curr

    # Return the neighboor of an exit if it's accessible
    # None otherise
    def __exitNeighboor(self, exit):
        if not self.__hasBottomWall(exit):
            return self.__getBottomItem(exit)

        if not self.__hasRightWall(exit):
            return self.__getRightItem(exit)

        if not self.__hasLeftWall(exit):
            return self.__getLeftItem(exit)

        if not self.__hasTopWall(exit):
            return self.__getTopItem(exit)

        return None

    # Preprocess the exits, cf. preprocessing()
    def __exitsPreprocessing(self, exitStart = None):
        # If we have already both of the exits...
        if len(self.__exitsDetermined) >= 2:
            return False
        
        if exitStart is not None:
            self.__exitsDetermined = [exitStart]
            numberOfExitsDetermined = 1
        else:
            self.__exitsDetermined = []
            numberOfExitsDetermined = 0

        whiteExits = []
        blackExits = []
            
        for node in self.__exits:
            if(self.__exitNeighboor(node) is None and not node.isPartiallyDetermined()):
                node.value = Type.leftWall + Type.rightWall + Type.bottomWall + Type.topWall
                self.__exits.remove(node)
            else:
                if (node.x + node.y)%2:
                    whiteExits.append(node)
                else:
                    blackExits.append(node)

            if node.isPartiallyDetermined() and not node in self.__exitsDetermined:
                numberOfExitsDetermined += 1
                self.__exitsDetermined.append(node)

        if len(whiteExits) == 1 and not whiteExits[0].isPartiallyDetermined():
            # There is only one white exit, if the number of cells is even, it's the only one
            # possible. If the number of cells is odd, it cannot be an exit (since we need 2 of them)
            if (self.__sizeX * self.__sizeY) % 2:
                whiteExits[0].value = Type.leftWall + Type.rightWall + Type.bottomWall + Type.topWall
                self.__exits.remove(whiteExits[0])
            else:
                self.__addSoftConstraint(whiteExits[0], self.__exitNeighboor(whiteExits[0]))

        if len(blackExits) == 1 and not blackExits[0].isPartiallyDetermined():
            # Same for black ones
            if (self.__sizeX * self.__sizeY) % 2:
                blackExits[0].value = Type.leftWall + Type.rightWall + Type.bottomWall + Type.topWall
                self.__exits.remove(blackExits[0])
            else:
                self.__addSoftConstraint(blackExits[0], self.__exitNeighboor(blackExits[0]))

        # If we found 2, close all the others (and remove them from the list of exits)
        if numberOfExitsDetermined >= 2:
            for node in self.__exits:
                if not node in self.__exitsDetermined:
                    node.value = Type.leftWall + Type.rightWall + Type.bottomWall + Type.topWall
            self.__exits = self.__exitsDetermined
            return True

        # If we found 1, close all the others that do not respect the property odd/even (and remove them from the list of exits)
        if numberOfExitsDetermined == 1:
            condition = (self.__sizeX * self.__sizeY) % 2
            for node in self.__exits:
                if node != self.__exitsDetermined[0]:
                    if (condition and (node.x + node.y)%2 != (self.__exitsDetermined[0].x + self.__exitsDetermined[0].y)%2)\
                        or(not condition and (node.x + node.y)%2 == (self.__exitsDetermined[0].x + self.__exitsDetermined[0].y)%2):
                        node.value = Type.leftWall + Type.rightWall + Type.bottomWall + Type.topWall
                        self.__exits.remove(node)
            return True

        return False

    def backtracking(self):
        if self.__exitsDetermined:
            aux = self.__backtrackingImpl(self.__exitsDetermined[0])
            if(aux):
                print('Trouvé avec ' + str(self.__exitsDetermined[0]))
        else:
            # Save all values
            saveExits = [(exit, exit.value) for exit in self.__exits]
            remainedExits = self.__exits[:]

            for exit in remainedExits:
                # # Cover all exits which can't be reached from this exit
                # self.__exitsPreprocessing(exit)
                # self.__currentVisitedExits = []
                # Do the backtrack
                aux = self.__backtrackingImpl(exit)
                if(aux):
                    print('Trouvé avec ' + str(exit))
                    break
                else:
                    # If not found, restore exits
                    # for savedExit, value in saveExits:
                    #     if not savedExit in self.__exits:
                    #         self.__exits.append(savedExit)
                    #         savedExit.value = value
                    print('Pas trouvé avec '+ str(exit))
    """
    Main algorithm of the program. Try to find a path from node (which must be an exit)
    to another exit by visiting all the other nodes only one time.
    Recursive algorithm. When the path is blocked (no more possibilities), the path
    is backtracked and try another solution from then.

    With the preprocessing, we also add those conditions to follow the already determined
    path from one node to another, speeding up the algorithm.

    New Backtraking conditions:
    -> Exits covered : If during the search, we manage to cover all the exits but the path is not found
    yet, it's useless to continue. We should go back 2 nodes before reaching the exit (the first one is
    in front of the exit, and the second one is its ancestor)
    """
    def __backtrackingImpl(self, node):

        # Visit the node and check if the end of the path is reached
        if(self.__visiting(node)):
            return True

        # if(Type.isExit(node.value)):
        #     if(not node in self.__currentVisitedExits):
        #         self.__currentVisitedExits.append(node)
        #     if len(self.__currentVisitedExits) == len(self.__exits):
        #         print("Hello")
        #         self.__currentVisitedExits.remove(node)
        #         self.__numberOfBacktrack = 1
        #         return False


        contraintFound = False

        # Test if there is a constraint
        if(node.isPartiallyDetermined()):
            if(node.nodeIn is not None):
                if(not node.nodeIn.visited):
                    contraintFound = True
                    saveNode = node.nodeIn
                    aux = self.__backtrackingImpl(node.nodeIn)
                    if aux:
                        node.nodeOut = saveNode
                        node.nodeOut.nodeIn = node
                        return True
            if(node.nodeOut is not None):
                if(not node.nodeOut.visited):
                    contraintFound = True
                    saveNode = node.nodeOut
                    aux = self.__backtrackingImpl(node.nodeOut)
                    if aux:
                        node.nodeOut = saveNode
                        node.nodeOut.nodeIn = node
                        return True


        if(not contraintFound):
            # Test the four things even if only max three of them can be true at once
            if(not self.__numberOfBacktrack and not self.__hasRightWall(node) and self.__getRightItem(node) is not None and not self.__getRightItem(node).visited):
                aux = self.__backtrackingImpl(self.__getRightItem(node))
                if aux:
                    node.nodeOut = self.__getRightItem(node)
                    node.nodeOut.nodeIn = node
                    return True

            if(not self.__numberOfBacktrack and not self.__hasLeftWall(node) and self.__getLeftItem(node) is not None and not self.__getLeftItem(node).visited):
                aux = self.__backtrackingImpl(self.__getLeftItem(node))
                if aux:
                    node.nodeOut = self.__getLeftItem(node)
                    node.nodeOut.nodeIn = node
                    return True

            if(not self.__numberOfBacktrack and not self.__hasTopWall(node) and self.__getTopItem(node) is not None and not self.__getTopItem(node).visited):
                aux = self.__backtrackingImpl(self.__getTopItem(node))
                if aux:
                    node.nodeOut = self.__getTopItem(node)
                    node.nodeOut.nodeIn = node
                    return True

            if(not self.__numberOfBacktrack and not self.__hasBottomWall(node) and self.__getBottomItem(node) is not None and not self.__getBottomItem(node).visited):
                aux = self.__backtrackingImpl(self.__getBottomItem(node))
                if aux:
                    node.nodeOut = self.__getBottomItem(node)
                    node.nodeOut.nodeIn = node
                    return True

        # We cannot progress, we unvisit this node and give the hand back to its ancestor
        self.__unvisiting(node)
        if(self.__numberOfBacktrack > 0):
            self.__numberOfBacktrack -=1 
        return False



# Test code
if __name__ == '__main__':

    

    # grid = Grid()

    # # Preallocation, should never happen
    # grid.hardInit(2,2)


    # node1 = Node(Type.leftWall, 0, 0)
    # node2 = Node(Type.rightWall + Type.topWall, 0, 1)
    # node3 = Node(Type.bottomWall + Type.exit, 1, 0)
    # node4 = Node(Type.bottomWall + Type.rightWall, 1, 1)

    # node1.nodeOut = node2
    # node2.nodeIn = node1
    # node2.nodeOut = node4
    # node3.nodeIn = node4
    # node4.nodeIn = node2
    # node4.nodeOut = node3

    # grid.setItem(0,0, node1)
    # grid.setItem(0,1, node2)
    # grid.setItem(1,0, node3)
    # grid.setItem(1,1, node4)

    # print(grid)

    # grid.writeToFile("level.txt")

    # grid2 = Grid()
    # grid2.readFromFile("level.txt")

    # print(grid2)

    grid3 = Grid("base.txt")

    grid3.printASCII()
    grid3.preprocessing(1)
    print(grid3)

    for exit in grid3.getExits():

        aux = grid3.backtracking(exit)

        if(aux):
            print('Trouvé avec ' + str(exit))
            print(grid3)
            break
        else:
            print('Pas trouvé avec '+ str(exit))



