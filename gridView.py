"""
Class GridView

All the GUI process to draw the grid and its path into a window.
Far more usefull to debug the program (and cleaner)
"""

from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter, QColor, QFont, QPainterPath, QBrush
from PyQt5.QtCore import Qt, QPoint
from type import Type

class GridView(QWidget):

    """
    Store different UI values. Can change them at will easily
    """
    def __init__(self, grid, levelName):
        super().__init__()

        self.__model = grid
        self.__tileSize = 50
        self.__wallThickness = 2.5
        self.__pathThickness = 5
        self.__levelName = levelName

        self.__initUI()

    # Set the window size to the size of the grid and move it to point (300,300) on the screen
    def __initUI(self):      
        self.setGeometry(300, 300, self.__model.getSizeY() * self.__tileSize, self.__model.getSizeX() * self.__tileSize)
        self.setWindowTitle(self.__levelName.title())
        self.show()

    # Event called to draw the grid
    def paintEvent(self, e):

        qp = QPainter()
        qp.begin(self)
        self.__draw(qp)
        qp.end()

    # Draw the grid
    def __draw(self, qp):
        col = QColor(0, 0, 0)
        col.setNamedColor('#d4d4d4')
        qp.setPen(col)

        for node in self.__model.getNodes():
            self.__drawGround(qp, node)
            qp.setPen(col)
            self.__drawWalls(qp, node)
            self.__drawPath(qp, node)
            # If it's an exit, we draw over the path our exit
            if Type.isExit(node.value):
                self.__drawExitGround(qp, node, QColor(0, 0, 0))

    def __drawGround(self, qp, node):
        black = QColor(0, 0, 0)
        grey = QColor(240, 240, 240)
        white = QColor(255, 255, 255)

        if Type.isBlock(node.value):
            qp.setBrush(black)
            qp.setPen(black)
        elif((node.x + node.y) % 2):
            qp.setPen(white)
            qp.setBrush(grey)
        else:
            qp.setPen(white)
            qp.setBrush(white)

        qp.drawRect(self.__tileSize * node.y, self.__tileSize * node.x, self.__tileSize, self.__tileSize)

    def __drawWalls(self, qp, node):
        qp.setBrush(QColor(100, 100, 100))
        qp.setPen(QColor(100, 100, 100))
        if(not Type.isBlock(node.value) and not Type.isExit(node.value)):
            if(Type.hasBottomWall(node.value)):
                qp.drawRect(self.__tileSize * node.y, self.__tileSize * (node.x + 1), self.__tileSize, self.__wallThickness)
            if(Type.hasTopWall(node.value)):
                qp.drawRect(self.__tileSize * node.y, self.__tileSize * (node.x), self.__tileSize, self.__wallThickness)
            if(Type.hasRightWall(node.value)):
                qp.drawRect(self.__tileSize * (node.y + 1) , self.__tileSize * node.x, self.__wallThickness, self.__tileSize)
            if(Type.hasLeftWall(node.value)):
                qp.drawRect(self.__tileSize * node.y, self.__tileSize * node.x , self.__wallThickness, self.__tileSize)

    def __drawPath(self, qp, node):
        qp.setBrush(QColor(0, 0, 240))
        qp.setPen(QColor(0, 0, 240))
        if Type.isBlock(node.value):
            return
        for other in [node.nodeIn, node.nodeOut]:
            if other is not None:
                if(other.x > node.x):
                    width = self.__pathThickness 
                    height = self.__tileSize/2 + self.__pathThickness/2
                    x = self.__tileSize * node.y + self.__tileSize/2 - self.__pathThickness/2
                    y = self.__tileSize * node.x + self.__tileSize/2 - self.__pathThickness/2
                elif(other.x < node.x):
                    width = self.__pathThickness
                    height = self.__tileSize/2 + self.__pathThickness/2
                    x = self.__tileSize * node.y + self.__tileSize/2 - self.__pathThickness/2
                    y = self.__tileSize * node.x
                else:
                    if(other.y > node.y):
                        height = self.__pathThickness
                        width = self.__tileSize/2
                        x = self.__tileSize * node.y + self.__tileSize/2
                        y = self.__tileSize * node.x + self.__tileSize/2 - self.__pathThickness/2
                    else:
                        height = self.__pathThickness
                        width = self.__tileSize/2
                        x = self.__tileSize * node.y
                        y = self.__tileSize * node.x + self.__tileSize/2 - self.__pathThickness/2
                qp.drawRect(x,y, width, height)



    def __drawExitGround(self, qp, node, color):
        path = QPainterPath()
        p1 = QPoint(node.y * self.__tileSize, node.x * self.__tileSize)
        p2 = QPoint((node.y+1) * self.__tileSize, node.x * self.__tileSize)
        p3 = QPoint((node.y+1) * self.__tileSize, (node.x+1) * self.__tileSize)
        p4 = QPoint(node.y * self.__tileSize, (node.x+1) * self.__tileSize)

        pCenter = QPoint((node.y+0.5) * self.__tileSize, (node.x+0.5) * self.__tileSize)

        path.moveTo(p1)
        if(not Type.hasTopWall(node.value)):
            path.lineTo(pCenter)
        path.lineTo(p2)
        if(not Type.hasRightWall(node.value)):
            path.lineTo(pCenter)
        path.lineTo(p3)
        if(not Type.hasBottomWall(node.value)):
            path.lineTo(pCenter)
        path.lineTo(p4)
        if(not Type.hasLeftWall(node.value)):
            path.lineTo(pCenter)
        path.lineTo(p1)

        qp.fillPath (path, QBrush(color));